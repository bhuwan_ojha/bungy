<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});



Route::group(['prefix' => 'bookings/'], function () {
    Route::post('/quote','BookingController@postBookingDate');
    Route::get('/user-info','BookingController@getUserInfoForm');
    Route::post('/user-info','BookingController@postUserInfo');
    Route::get('/confirm','BookingController@getConfirmation');
    Route::post('/thankyou','BookingController@postEmailConfirmation');


});