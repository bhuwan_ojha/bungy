<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bungy Adventure Nepal</title>

    <!-- Bootstrap -->
    <link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!--icons-->
    <link href="{{url('assets/vendor/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!--Slick Carousel-->
    <link href="{{url('assets/vendor/slick-carousel/slick/slick.css')}}" rel="stylesheet">
    <link href="{{url('assets/vendor/slick-carousel/slick/slick-theme.css')}}" rel="stylesheet">


    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">

    <!--Custom Styles-->
    <link rel="stylesheet" href="{{url('assets/style/master.css')}}">

    <link rel="stylesheet" href="{{url('assets/js/lib/themes/default.css')}}">
    <link rel="stylesheet" href="{{url('assets/js/lib/themes/default.date.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
</head>
<body>

<div class="top-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="list-inline locate">
                    <li><span class="ion-android-locate"></span></li>
                    <li>Pardi 6,<a href=""> Pokhara-lekhnath ,Nepal</a></li>

                    <li><span class="ion-android-phone-portrait"></span></li>
                    <li>Call Us: <a href="">+01-234-567-890 </a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <ul class="list-inline social">
                    <li><a href=""><span class="">Login</span></a></li>
                    <li><a href=""><span class="">Register</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default navbar-custom">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/assets/images/logo/logo.png" alt="logo">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Home</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Works</a></li>
                <li><a href="#">Achivements</a></li>
                <li><a href="#">Branch</a></li>
                <li><a href="#booking-section" class="btn btn-outline-black js-anchor-link">Book Jump</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>

@yield('content')


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li><img src="/assets/images/logo/logo.png" alt="bungy-logo"></li>
                    <li class="description">
                        <p>Bungy Adventure Nepal</p>
                    </li>
                </ul>
                <ul class="list-inline social">
                    <li><a href=""><span class="ion-social-facebook"></span></a></li>
                    <li><a href=""><span class="ion-social-twitter"></span></a></li>

                </ul>
            </div>
            <div class="col-md-3">
                <h4>Other Links</h4>
                <ul class="list-unstyled bod">
                    <li><a href="">FAQ</a></li>
                    <li><a href="">Terms & Conditions</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Upcoming Events</a></li>
                    <li><a href="">Certifications</a></li>

                </ul>
            </div>
            <div class="col-md-5">
                <h4>Contact Us</h4>
                <ul class="list-unstyled bod">
                    <li><span class="">Location</span> Pardi 6 , Pokhara Lekhnath</li>
                    <li><span class="">Office</span> Pardi 6 , Pokhara Lekhnath</li>
                    <li><span class="">Phone</span> +123-456-789 ,  +123-456-789</li>
                </ul>

            </div>
        </div>
    </div>
</footer>


<script src="{{url('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/vendor/slick-carousel/slick/slick.min.js')}}"></script>
<script src="{{url('assets/js/lib/picker.js')}}"></script>
<script src="{{url('assets/js/lib/picker.date.js')}}"></script>
<script src="{{url('assets/js/lib/picker.time.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

<!--homepage slider-->
<script>
    $('.banner-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        cssEase: 'linear'
    });

</script>
<script>
    $('.services-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000
    });

</script>
<script>
    $('.squad-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000
    });

</script>
<script>
    $('.work-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000
    });

</script>
<script>
    let today = new Date().toISOString().substr(0, 10);
    document.querySelector("#today").value = today;

    document.querySelector("#today2").valueAsDate = new Date();
</script>
<script>
    //plugin bootstrap minus and plus
    //http://jsfiddle.net/laelitenetwork/puJ6G/
    $('.btn-number').click(function(e){
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script>
    $('.js-anchor-link').click(function(e){
        e.preventDefault();
        var target = $($(this).attr('href'));
        if(target.length){
            var scrollTo = target.offset().top;
            $('body, html').animate({scrollTop: scrollTo+'px'}, 800);
        }
    });
</script>
<script>
    $(function () {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            min: true,
            max: false
        });
        $('#quantity').keypress(function(e) {
            e.preventDefault();
        });

        $('#confirm-form').validate();
        $('#user-info').validate();
    });

</script>

</body>
</html>