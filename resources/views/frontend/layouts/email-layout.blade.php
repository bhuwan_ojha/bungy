<div class="bg-main">
    <div class="container">
        <div class="section header">
            <table>
                <tr>
                    <td>
                        <h4><img src="{{ url('assets/images/logo/logo.png') }}" alt="{{env('SITE_NAME')}}"
                                 style="width:50%;">
                        </h4>
                    </td>

                    <td class="text-right">
                        <h4>Email: <a
                                    href="mailto:{{env('SITE_EMAIL')}}"></a>{{env('SITE_EMAIL')}}
                        </h4>
                        <h4>Phone: <a
                                    href="tel:{{env('SITE_NUMBER')}}"></a>{{env('SITE_NUMBER')}}
                        </h4>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@yield('content')
<div>
    <div class="container">
        <div class="section footer">
            <div class="footer-links">
                <p>
                    &copy; <?= date('Y') ?> Copyright:<a href="/">{{env('SITE_EMAIL')}}</a>
                </p>
            </div>
        </div>
    </div>
</div>