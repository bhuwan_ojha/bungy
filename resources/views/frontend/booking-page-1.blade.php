@extends('frontend.layouts.layout')
@section('content')
<section class="inner-booking">
        <div class="container">
            <form action="/bookings/user-info" method="post" id="user-info" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="selection-form-group">
                        <div class="heading-text">
                            <h1>Your Jump</h1>
                            <h4>Online booking form & details</h4>
                        </div>
                        <div class="selected">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-white">
                                        <label for="">Your Date Selected </label>
                                        <input  name="jump_date" required value="{{$quote['jump_date']}}" autocomplete="off" type="text" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-white">
                                        <label for="">No of people</label>
                                        <div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number"  disabled="disabled" data-type="minus"
                      data-field="quantity">
                  <span class="glyphicon glyphicon-minus"></span>
              </button>
          </span>
                                            <input type="text" name="quantity" required id="quantity" autocomplete="off" class="form-control input-number" value="{{$quote['quantity']}}" min="1"
                                                   max="3">
                                            <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
                  <span class="glyphicon glyphicon-plus"></span>
              </button>
          </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <p>Note: Your number of selected person for jump details must be filled on the form. After you sucessfully registered your booking you will be provided a short notification via your SMS and email you have provided  </p>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div id="exTab2" class="person-tabs">
                        <ul class="nav nav-tabs">

                            @php($quantity = $quote['quantity'])
                            @for($x = 1; $x <= $quantity; $x++ )
                            <li @if($x=='1')class="active" @endif>
                                <a  href="#{{$x}}" data-toggle="tab">Person {{$x}}</a>
                            </li>
                            @endfor
                        </ul>

                        <div class="tab-content ">
                            <div class="tab-pane active" id="1">
                                <div class="row">
                                    <div class="col-md-6"><div class="form-group">
                                            <label for="">Select Type</label>
                                            <select required name="user_type[]" id="" class="form-control">
                                                <option value="">Select Nationality Type</option>
                                                <option value="Nepalese"> Nepalese</option>
                                                <option value="Foreigner"> Foreigner</option>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Citizenship/ Passport ID</label>
                                            <input type="text" required name="citizenship_number[]" class="form-control">
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text"  required name="first_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" required name="last_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <select required name="gender[]" id="" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Do not Specify">Do not Specify</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Age</label>
                                            <input type="number" required name="age[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Blood Group</label>
                                            <select required name="blood_group[]" id="" class="form-control">
                                                <option value="">Select Blood Group</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O-">O-</option>
                                                <option value="O+">O+</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="">Upload Citizenship / Passport ( Front Only)</label>
                                    <input type="file" required name="citizenship_front[]" class="">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="buttons">
                                        <button class="btn btn-black full-width" type="submit">CONTINUE</button>
                                    </div>
                                </div>
                            </div>
                            @if($quote['quantity'] >=2)
                            <div class="tab-pane active" id="2">
                                <a href="" class=" remove text-right">Remove Person</a>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6"><div class="form-group">
                                            <label for="">Select Type</label>
                                            <select required name="user_type[]" id="" class="form-control">
                                                <option value="">Select Nationality Type</option>
                                                <option value="Nepalese"> Nepalese</option>
                                                <option value="Foreigner"> Foreigner</option>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Citizenship/ Passport ID</label>
                                            <input type="text" required name="citizenship_number[]" class="form-control">
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text"  required name="first_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" required name="last_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <select required name="gender[]" id="" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Do not Specify">Do not Specify</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Age</label>
                                            <input type="number" required name="age[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Blood Group</label>
                                            <select required name="blood_group[]" id="" class="form-control">
                                                <option value="">Select Blood Group</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O-">O-</option>
                                                <option value="O+">O+</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="">Upload Citizenship / Passport ( Front Only)</label>
                                    <input type="file" required name="citizenship_front[]" class="">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="buttons">
                                        <button class="btn btn-black full-width" type="submit">CONTINUE</button>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($quote['quantity']==3)
                            <div class="tab-pane active" id="3">
                                <a href="" class=" remove text-right">Remove Person</a>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6"><div class="form-group">
                                            <label for="">Select Type</label>
                                            <select required name="user_type[]" id="" class="form-control">
                                                <option value="">Select Nationality Type</option>
                                                <option value="Nepalese"> Nepalese</option>
                                                <option value="Foreigner"> Foreigner</option>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Citizenship/ Passport ID</label>
                                            <input type="text" required name="citizenship_number[]" class="form-control">
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text"  required name="first_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" required name="last_name[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <select required name="gender[]" id="" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Do not Specify">Do not Specify</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Age</label>
                                            <input type="number" required name="age[]" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Blood Group</label>
                                            <select required name="blood_group[]" id="" class="form-control">
                                                <option value="">Select Blood Group</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O-">O-</option>
                                                <option value="O+">O+</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="">Upload Citizenship / Passport ( Front Only)</label>
                                    <input type="file" required name="citizenship_front[]" class="">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="buttons">
                                        <button class="btn btn-black full-width" type="submit">CONTINUE</button>
                                    </div>
                                </div>
                            </div>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
</section>
@endsection
