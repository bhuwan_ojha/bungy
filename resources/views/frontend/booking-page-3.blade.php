@extends('frontend.layouts.layout')
@section('content')
<section class="inner-booking">
        <div class="container">

            <div class="selection-form-group">
                <div class="heading-text">
                    <h1>Booking ID #{{$userData['ref_id']}}</h1>
                    <h4>Your Jump Has Been Successfully Booked</h4>
                </div>
                <div class="description">
                    <p style="text-align: left">Dear {{$userData['first_name']}} {{$userData['last_name']}}, Your booking is in the progress you will be shortly notified with your email({{$data['email']}}).</p>
                    <p style="text-align: left">If Something went wrong please try to contact our office at number {{env('SITE_NUMBER')}} or via {{env('SITE_EMAIL')}}</p>

                </div>
                <div class="buttons">
                    <a href="" class="btn btn-black"> View My Account</a>
                </div>
            </div>
        </div>
</section>
@endsection
