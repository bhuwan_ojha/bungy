@extends('frontend.layouts.layout')
@section('content')
<section class="inner-booking">
        <div class="container">
            <form action="/bookings/thankyou" method="post" id="confirm-form">
                {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="selection-form-group">
                        <div class="heading-text">
                            <h1>Rs {{$total}} </h1>
                            <h4>Your Price Break Down</h4>
                        </div>
                        <div class="description">

                            @foreach($bookingData as $key=>$bookingDataValue)
                            <div class="person">
                                <div class="row">
                                    <div class="col-md-8">
                                        <ul class="list-unstyled">
                                            <li>{{$bookingDataValue->first_name}} {{$bookingDataValue->last_name}} ( {{$bookingDataValue->user_type}} )</li>
                                            <li>{{$bookingDataValue->gender}} | {{$bookingDataValue->age}}yrs | {{$bookingDataValue->blood_group}}</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <ul class="list-unstyled">
                                            <li>{{$discountedPrice[$key]['discount']}}%</li>
                                            <li class="price">NRP  {{$discountedPrice[$key]['price']}}</li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="booking_id[]" value="{{$bookingDataValue->id}}" required class="form-control">

                                </div>
                                <div class="actions">
                                    <ul class="list-inline text-right">
                                        <a href=""><span class="">Remove Person</span></a>
                                    </ul>
                                </div>
                            </div>
                                @endforeach
                        </div>

                    </div>
                </div>
                <div class="col-md-6">

                            <div class="tab-content confirm">
                                <div class="heading-text">
                                    <h3>Confirm Your Jump</h3>
                                </div>
                                <p>Your Jump is one click away from being booked , Please enter Your email & phone number to confirm</p>
                                <hr>
                                <div class="form-group">
                                    <label for="">Your Email Address</label>
                                    <input type="email" name="email" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Your Phone Number</label>
                                    <input type="Phone" name="phone" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="terms" required >
                                    By clicking this i accept all the <a href="">Terms & Conditions</a>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="buttons">
                                        <button class="btn btn-black full-width"> CONFIRM JUMP NOW </button>
                                    </div>

                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
</section>
@endsection
