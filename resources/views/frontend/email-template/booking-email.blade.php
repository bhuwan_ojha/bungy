@extends('frontend.layouts.email-layout')
@section('content')
<table class="bg-main">
    <tr>
        <td>
            <table class="container">
                <tr>
                    <td class="section">

                        <table class="table-details">

                            <tbody>
                            @if (isset($data['data']['recipient']) && $data['data']['recipient'] == 'Admin')
                            <tr>
                                <td colspan="2">

                                    <p> <strong>Dear Admin</strong><br>
                                        Following are the online booking details. </p>
                                </td>
                            </tr>
                            @else
                                <tr>
                                    <td colspan="2">

                                            <p> Dear {{$data['userData']['first_name']}} {{$data['userData']['last_name']}},</p> <br/>
                                            <p>Thank you for your booking.We have received your booking details.We'll
                                                confirm your booking shortly.</p>
                                        @endif
                                    </td>
                                </tr>


                            <tr>
                                <th colspan="2" class="table-details-title">Jumpers Details</th>
                            </tr>
                            @if(isset($data['userData']['ref_id']))
                                <tr>
                                    <th width="30%">Booked RefID</th>
                                    <td>{{$data['userData']['ref_id']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['userData']['first_name'])&&isset($data['userData']['last_name']))
                                <tr>
                                    <th width="30%">Name</th>
                                    <td>{{$data['userData']['first_name']}} {{$data['userData']['last_name']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['data']['email']))
                                <tr>
                                    <th>Email</th>
                                    <td>{{$data['data']['email']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['data']['phone']))
                                <tr>
                                    <th>Cell No.</th>
                                    <td>+{{$data['data']['phone']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['userData']['user_type']))
                                <tr>
                                    <th>Nationality Type</th>
                                    <td>{{$data['userData']['user_type']}}</td>
                                </tr>
                            @endif
                                @if(isset($data['userData']['citizenship_number']))
                                    <tr>
                                        <th>Citizenship Number</th>
                                        <td>{{$data['userData']['citizenship_number']}}</td>
                                    </tr>
                                @endif
                                @if ($data['data']['recipient'] == 'Admin' && $data['userData']['citizenship_front'])
                                    <tr>
                                        <th>Citizenship Picture</th>
                                        <td><img src="/uploads/citizenship/"{{$data['userData']['citizenship_front']}}></td>
                                    </tr>
                                @endif
                                    <tr>
                                <th colspan="2" class="table-details-title">Bungy Details</th>
                            </tr>
                            @if(isset($data['userData']['jump_date']))
                                <tr>
                                    <th width="30%">Date</th>
                                    <td>{{$data['userData']['jump_date']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['userData']['quantity']))
                                <tr>
                                    <th>No. of Customer</th>
                                    <td>{{$data['userData']['quantity']}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Final Fare</th>
                                {{--<td>@if(isset($total_fare)){{$data['userData']['total']}}@endif</td>--}}
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


@endsection