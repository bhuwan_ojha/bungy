@extends('frontend.layouts.layout')
@section('content')
<section class="main-banner">
    <div class="banner-slider">
        <div class="banner">
            <img src="assets/images/banner-bungy.jpg" alt="bungy-banner1" class="img-responsive">
        </div>
        <div class="banner">
            <img src="assets/images/banner-bungy.jpg" alt="bungy-banner2" class="img-responsive">
        </div>
        <div class="banner">
            <img src="assets/images/banner-bungy.jpg" alt="bungy-banner3" class="img-responsive">
        </div>
        <div class="quote-wrapper">
            <div class="container">
                <div class="form-group">

                </div>
            </div>
        </div>
    </div>

</section>
<section class="bungy-services">
    <div class="container">
        <div class="services-slider">
            <div class="services">
                <div class="services-inner-wrapper">
                    <figure>
                        <img src="assets/images/services/icons/003-cup.png" alt="bungy-service">
                    </figure>
                    <div class="caption-text">
                        <h4>Well Trained Jump Master</h4>
                        <p>13+ Years Of Experience Over Bungy Jump</p>
                    </div>
                </div>
            </div>
            <div class="services darken-service">
                <div class="services-inner-wrapper">
                    <figure>
                        <img src="assets/images/services/icons/002-camera.png" alt="bungy-service">
                    </figure>
                    <div class="caption-text">
                        <h4>High Quality Images & Video</h4>
                        <p>Providing You better qualities of your memorable jumps</p>
                    </div>
                </div>
            </div>
            <div class="services">
                <div class="services-inner-wrapper">
                    <figure>
                        <img src="assets/images/services/icons/001-price.png" alt="bungy-service">
                    </figure>
                    <div class="caption-text">
                        <h4>Low Price Well Secured Jumps</h4>
                        <p>Cost effective low competitive prices over experience</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="into-bungy">
            <div class="row">
                <div class="col-md-5">
                    <div class="heading-text">
                        <h3>What Is The</h3>
                        <h4>BUNGY NEPAL ADVENTURE</h4>
                    </div>

                </div>
                <div class="col-md-7">
                    <div class="description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, accusamus consequatur cum
                            debitis deleniti, dolorum ducimus enim est incidunt inventore magnam minima mollitia, nam
                            placeat possimus repellat tenetur vel voluptatem.</
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequuntur dignissimos
                            dolorum ea fuga neque nobis odit, omnis pariatur ratione rem repellat sed similique ut vero.
                            Consequuntur ducimus illum natus!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bungy-squad">
    <div class="container">
        <div class="heading-text">
            <h3>Meet The</h3>
            <h4>Bungy Squad</h4>
        </div>
        <div class="description">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis</p>
        </div>
        <div class="squad-slider">
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="squad">
                <div class="squad-inner-wrapper">
                    <figure>
                        <img src="assets/images/sqads/squad.png" alt="bungy-nepal-sqad">
                    </figure>
                    <div class="caption-text">
                        <ul class="list-unstyled">
                            <li class="name">
                                Santosh Bhattarai
                            </li>
                            <li class="title">
                                Jump Master
                            </li>
                            <li class="exp">
                                13 years of experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bungy-quote" id="booking-section">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="bungy-prepare">
                    <div class="heading-text">
                        <h3>It's Time</h3>
                        <h4>To Make A Jump</h4>
                    </div>
                    <div class="description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="icons-action">
                        <ul class="list-inline">
                            <li><a href="">Visit Gallery</a></li>
                            <li><a href=""><span class="ion-ios-play-outline"></span> Watch Video</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="quote-section ">
                    <form action="/bookings/quote" method="post">
                        {{csrf_field()}}
                        <h3>Pick Your <br> Date For The Jump</h3>
                        <div class="form-group">
                            <label for="">Your Date </label>
                            <input id="today" autocomplete="off" name="jump_date" type="text" class="form-control datepicker">
                        </div>
                        <div class="form-group">
                            <label for="">No of people</label>
                            <div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus"
                      data-field="quantity">
                  <span class="glyphicon glyphicon-minus"></span>
              </button>
          </span>
                                <input type="text" autocomplete="off" name="quantity" id="quantity" class="form-control input-number" value="1" min="1"
                                       max="3">
                                <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
                  <span class="glyphicon glyphicon-plus"></span>
              </button>
          </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="buttons">
                                <button class="btn btn-black"> BOOK MY JUMP</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bungy-works">
    <div class="container">
        <div class="heading-text">
            <h3>See Our</h3>
            <h4>Latest Events</h4>
        </div>
        <div class="work-slider">
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bungee-packages">
    <div class="container">
        <div class="heading-text  padding-bottom text-center">
            <h3>Recommended</h3>
            <h4>Jumping Packages</h4>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="price">
                    <div class="heading-text text-center">
                        <h4>Starting With</h4>
                    </div>
                    <ul class="list-unstyled">
                        <li>
                            <span><img src="assets/images/icons/hd-sign.svg" alt="bungy-hd-video"></span>
                        </li>
                        <li>Full HD ( 1080p)</li>
                        <li>Video length: max 2- 3 mins</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price price-darken">
                    <div class="heading-text text-center">
                        <h4>Starting With</h4>
                    </div>
                    <ul class="list-unstyled">
                        <li>
                            <span><img src="assets/images/icons/hd-sign.svg" alt="bungy-hd-video"></span>
                        </li>
                        <li>Full HD ( 1080p)</li>
                        <li>Video length: max 2- 3 mins</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price">
                    <div class="heading-text text-center">
                        <h4>Starting With</h4>
                    </div>
                    <ul class="list-unstyled">
                        <li>
                            <span><img src="assets/images/icons/hd-sign.svg" alt="bungy-hd-video"></span>
                        </li>
                        <li>Full HD ( 1080p)</li>
                        <li>Video length: max 2- 3 mins</li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
</section>

@endsection
