<?php
namespace App\Services;
use App\Libraries\TMHelper;
use Mail;
use Pelago\Emogrifier;

class MailService
{

    private function emogrify($path, $data)
    {
        $emogrifier = new Emogrifier();
        $emogrifier->setHtml(view($path, compact('data')));
        $emogrifier->setCss(file_get_contents(app_path() . '../../public/emailer/newcss.css'));
        $emogrifier->setCss(file_get_contents(app_path() . '../../public/emailer/email-template.css'));
        $contactHtml = $emogrifier->emogrify();

        return $contactHtml;
    }


    public function sendContactEmail($data)
    {
        $html = $this->emogrify('frontend.email-template.contact-email', $data);

        return Mail::send(
            [],
            [],
            function ($message) use ($data, $html) {
                $message->from(env('SITE_EMAIL'), env('SITE_NAME'));
                $message->to(env('ADMIN_EMAIL'));
                $message->subject('Contact Email Received');
                $message->setBody($html, 'text/html');
            }
        );
    }

    public function sendBookingEmail($data, $userData)
    {
        $data['recipient'] = 'Admin';
        $emaileTemplate =  $this->emogrify('frontend.email-template.booking-email', compact('userData','data'));
        $this->sendEmail(
            $emaileTemplate,
            $receiver = env('ADMIN_EMAIL'),
            $subject = "Reservation received"
        );
        $data['recipient'] = 'Customer';
        $emaileTemplate =  $this->emogrify('frontend.email-template.booking-email', compact('userData','data'));
        $this->sendEmail(
            $emaileTemplate,
            $receiver = $data['email'],
            $subject = "Reservation received"
        );


    }

    public function sendEmail($body, $receiver, $subject)
    {
        try {
            Mail::send([], [], function ($message) use ($receiver, $body, $subject) {
                $message->from(env('SITE_EMAIL'));
                $message->to($receiver);
                $message->subject($subject);
                $message->setBody($body, 'text/html');
            });
        } catch (\exception $e) {
            return false;
        }

        return true;
    }
}


