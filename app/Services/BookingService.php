<?php

namespace App\Services;

use App\Booking;
use App\Price;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Session;
use Image;

class BookingService
{

    private $booking;

    public function __construct(Booking $booking, MailService $mailService, Price $price)
    {
        $this->booking = $booking;
        $this->mail = $mailService;
        $this->price = $price;
    }

    public function saveUpdate($bookingData, $id = null)
    {
        DB::beginTransaction();
        try {
            unset($bookingData['_token']);

            $citizenshipImage = isset($bookingData['citizenship_front']) ? $bookingData['citizenship_front'] : '';
            for ($i = 0; $i < count($bookingData['first_name']); $i++) {
                if (isset($citizenshipImage[$i]) && $citizenshipImage[$i] !== null) {
                    $image[] = $citizenshipImage[$i];
                    $input['imagename'][$i] = time() . '.' . $image[$i]->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/citizenship/');
                    $image[$i]->move($destinationPath, $input['imagename'][$i]);
                    $citizenshipImage[$i] = $input['imagename'][$i];
                    $new_booking_data = array(
                        "jump_date" => $bookingData['jump_date'],
                        "quantity" => $bookingData['quantity'],
                        "user_type" => $bookingData['user_type'][$i],
                        "citizenship_number" => $bookingData['citizenship_number'][$i],
                        "first_name" => $bookingData['first_name'][$i],
                        "last_name" => $bookingData['last_name'][$i],
                        "gender" => $bookingData['gender'][$i],
                        "age" => $bookingData['age'][$i],
                        "blood_group" => $bookingData['blood_group'][$i],
                        "citizenship_front" => $citizenshipImage[$i]
                    );
                } else {
                    unset($citizenshipImage[$i]);
                }

                    $new_booking_data['ref_id'] = $this->refIdGenerate();
                    $savebooking[] = $this->booking->create($new_booking_data);
                    DB::commit();

            }
            return $savebooking;


        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function updateEmailPhone($bookingData,$userId)
    {
        try {
            foreach ($userId as $id) {
                $savebooking = $this->booking->where(['id' => $id])->update($bookingData);
            }
            return $savebooking;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function refIdGenerate()
    {
        $ref_id = rand(10000, 99999);
        $booking = Booking::where('ref_id', $ref_id)->first();
        if (!$booking) {
            return $ref_id;
        } else {
            refIdGenerate();
        }
    }

    public function getPriceByUserType($userType)
    {
        $discount = 0;
        $definedPrice = $this->price->first();
        $price = $definedPrice['price'];
        if ($userType !== 'Foreigner') {
            $discount = 15;
            $price -= $price * $discount / 100;
        }

        return compact('discount', 'price');
    }


}