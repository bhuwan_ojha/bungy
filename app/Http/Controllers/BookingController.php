<?php

namespace App\Http\Controllers;

use App\Services\BookingService;
use App\Services\MailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Image;

class BookingController extends Controller
{

    private  $booking;
    private  $mail;
    public function __construct(BookingService $bookingService , MailService $mailService)
    {
        $this->booking = $bookingService;
        $this->mail = $mailService;
    }

    public function postBookingDate(Request $request)
    {
        $request->validate([
            'jump_date' => 'required',
            'quantity' => 'required|max:3',
        ]);
        $quoteData = [
            'jump_date' => $request->jump_date,
            'quantity' =>$request->quantity
        ];

        Session::put('quoteData', $quoteData);

        return redirect('bookings/user-info');


    }

    public function getUserInfoForm()
    {
        $quote = Session::get('quoteData');
        return view('frontend.booking-page-1',compact('quote'));
    }

    public function postUserInfo(Request $request)
    {

        $request->validate([
            'jump_date' => 'required',
            'quantity' => 'required|max:3',
            'user_type' => 'required',
            'citizenship_number' => 'required',
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'gender' => 'required',
            'age' => 'required|max:60',
            'blood_group' => 'required',
        ]);
        $data = $request->all();
        $bookingData = $this->booking->saveUpdate($data);
        Session::put('bookingData',$bookingData);
        return redirect('bookings/confirm');
    }

    public function getConfirmation()
    {
        $bookingData = Session::get('bookingData');
        foreach($bookingData as $value) {
            $price = $this->booking->getPriceByUserType($value['user_type']);
            $discountedPrice[] = $price;
        }
        $total = 0;
        foreach ($discountedPrice as $item) {
            $total += $item['price'];
        }
       return view('frontend.booking-page-2',compact('bookingData','discountedPrice','total'));
    }

    public function postEmailConfirmation(Request $request)
    {
        $userData = Session::get('userData');
        $userId = $request->booking_id;
        $data = $request->except('booking_id','_token','terms');
        $this->booking->updateEmailPhone($data,$userId);
        $this->mail->sendBookingEmail($data,$userData);
        return view('frontend.booking-page-3',compact('userData','data'));
    }
}
