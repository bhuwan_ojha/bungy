<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function __construct()
    {

    }

    public function getPage($slug = null)
    {
        switch ($slug) {
            case '':
                return view('frontend.index');
                break;
            default:
                if (View::exists('frontend.'.$slug)) {
                    return view('frontend.'.$slug);
                }
        }

        return view('frontend.404');
    }
}
